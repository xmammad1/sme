package fi.muni.cz.sme.doctor.rest;


import fi.muni.cz.sme.doctor.api.responsedto.DoctorResponseDto;
import fi.muni.cz.sme.doctor.service.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/doctors")
public class DoctorController {

    private final DoctorService doctorService;

    @GetMapping("/{id}")
    public DoctorResponseDto getById(@PathVariable Long id) {
        return doctorService.getDoctorById(id);
    }

    @GetMapping
    public List<DoctorResponseDto> getAll() {
        return doctorService.getAllDoctors();
    }

    @GetMapping("/speciality/{speciality}")
    public List<DoctorResponseDto> getBySpeciality(@PathVariable String speciality) {
        return doctorService.getDoctorBySpeciality(speciality);
    }

    @GetMapping("/language/{language}")
    public List<DoctorResponseDto> getByLanguage(@PathVariable String language) {
        return doctorService.getDoctorByLanguage(language);
    }

    @GetMapping("/speciality/{speciality}/language/{language}")
    public List<DoctorResponseDto> getBySpecialityAndLanguage(@PathVariable String speciality, @PathVariable String language) {
        return doctorService.getDoctorsBySpecialityAndLanguage(speciality, language);
    }

    @GetMapping("/longitude/{longitude}/latitude/{latitude}")
    public List<DoctorResponseDto> getByLongitudeAndLatitude(@PathVariable Double longitude, @PathVariable Double latitude) {
        return doctorService.getDoctorByLongitudeAndLatitude(longitude, latitude);
    }

    @GetMapping("/address/{address}")
    public List<DoctorResponseDto> getByAddress(@PathVariable String address) {
        return doctorService.getDoctorByAddress(address);
    }

}
