package fi.muni.cz.sme.doctor.mapper;

import fi.muni.cz.sme.doctor.api.responsedto.DoctorResponseDto;
import fi.muni.cz.sme.doctor.model.Doctor;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import javax.print.Doc;
import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface DoctorMapper {

    DoctorResponseDto toResponseDto(Doctor doctor);

    List<DoctorResponseDto> toResponseDtoList(List<Doctor> doctors);

}
