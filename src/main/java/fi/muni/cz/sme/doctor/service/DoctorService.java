package fi.muni.cz.sme.doctor.service;

import fi.muni.cz.sme.doctor.api.responsedto.DoctorResponseDto;
import fi.muni.cz.sme.doctor.exception.EntityNotFoundException;
import fi.muni.cz.sme.doctor.mapper.DoctorMapper;
import fi.muni.cz.sme.doctor.model.Doctor;
import fi.muni.cz.sme.doctor.repository.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DoctorService {

    private final DoctorRepository doctorRepository;
    private final DoctorMapper doctorMapper;


    @Transactional
    public DoctorResponseDto getDoctorById(Long id) {
        return doctorMapper.toResponseDto(doctorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Doctor.class, id)));
    }

    @Transactional
    public List<DoctorResponseDto> getAllDoctors() {
        return doctorMapper.toResponseDtoList(doctorRepository.findAll());
    }

    @Transactional
    public List<DoctorResponseDto> getDoctorBySpeciality(String specialty) {
        return doctorMapper.toResponseDtoList(doctorRepository.findAllBySpecialty(specialty));
    }

    @Transactional
    public List<DoctorResponseDto> getDoctorsBySpecialityAndLanguage(String specialty, String language) {
        return doctorMapper.toResponseDtoList(doctorRepository.findAllBySpecialtyAndLanguages(specialty, language));
    }

    @Transactional
    public List<DoctorResponseDto> getDoctorByLanguage(String language) {
        return doctorMapper.toResponseDtoList(doctorRepository.findAllByLanguages(language));
    }

    @Transactional
    public List<DoctorResponseDto> getDoctorByLongitudeAndLatitude(Double longitude, Double latitude) {
        return doctorMapper.toResponseDtoList(doctorRepository.findAllByLongitudeAndLatitude(longitude, latitude));
    }

    @Transactional
    public List<DoctorResponseDto> getDoctorByAddress(String address) {
        return doctorMapper.toResponseDtoList(doctorRepository.findAllByAddress(address));
    }

}
