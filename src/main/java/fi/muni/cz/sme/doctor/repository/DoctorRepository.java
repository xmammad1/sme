package fi.muni.cz.sme.doctor.repository;

import fi.muni.cz.sme.doctor.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    List<Doctor> findAllByLanguages(String language);
    List<Doctor> findAllBySpecialty(String specialty);
    List<Doctor> findAllBySpecialtyAndLanguages(String specialty, String language);

    List<Doctor> findAllByLongitudeAndLatitude(Double longitude, Double latitude);

    List<Doctor> findAllByAddress(String address);

}
