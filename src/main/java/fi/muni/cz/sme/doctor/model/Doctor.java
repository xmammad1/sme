package fi.muni.cz.sme.doctor.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Doctor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String specialty;
    String name;
    @ElementCollection
    List<String> languages;
    String address;
    String phone;
    String email;
    String web;
    Double latitude;
    Double longitude;



}
