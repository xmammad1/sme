package fi.muni.cz.sme.doctor.api.responsedto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DoctorResponseDto {

    Long id;
    String specialty;
    String name;
    List<String> languages;
    String address;
    String phone;
    String email;
    String web;
    Double latitude;
    Double longitude;

}
